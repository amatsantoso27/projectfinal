import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Splash from './screen/Splash.js';
import Register from './screen/Register';
import Login from './screen/Login';
import Movie from './screen/Movie';
import About from './screen/AboutScreen'
import DetailMovie from './screen/DetailMovie';
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();


const TabsScreen = () => (
  <Tabs.Navigator
    tabBarOptions={{
      activeTintColor: '#e91e',
    }}>
    <Tabs.Screen
      name="Home"
      component={Movie}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color, size}) => (
          <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
      }}
    />
    <Tabs.Screen
      name="About"
      component={About}
      options={{
        tabBarLabel: 'About',
        tabBarIcon: ({color, size}) => (
          <Icon name="user-circle" color={color} size={size} />
        ),
      }}
    />
  </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home">
    <Drawer.Screen name="Home" component={Movie} />
    <Drawer.Screen name="About" component={About} />
  </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="Splash">
    <RootStack.Screen
      name="Splash"
      component={Splash}
      options={{headerShown: false}}
    />
    <RootStack.Screen name="Register" component={Register} />
    <RootStack.Screen name="Login" component={Login} />

    {/* <RootStack.Screen name="App" component={DrawerScreen} /> */}
  </RootStack.Navigator>
);

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Register" component={Register} options={{headerShown: false}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
        <Stack.Screen name="DetailMovie" component={DetailMovie} options={{headerShown: false}}/>
        <Stack.Screen name="Home" component={TabsScreen} options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
