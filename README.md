# ProjectFinal

Keterangan aplikasi: 
Aplikasi ini dibuat untuk memenuhi tugas final projek sanbercode dalam kelas React Native Mobile App Development.
Aplikasi ini terdiri dari 5 screen yang saling terhubung.
1. Splash screen, yang muncul saat pertama kali aplikasi dibuka
2. Register screen, yang muncul setelah splash screen ditampilkan. Dan berisi kolom inputan untuk register. Dan apabila ditekan tombol sign up maka akan menuju ke halaman movie
3. Login screen, yang muncul apabila tombol sign in di register screen ditekan. 
4. Movie screen, yang muncul setelah tombol sign up di tekan pada register screen, atau tombol sign in, google, dan facebook di tekan pada login screen.
Movie screen berisi list film yang populer saat ini.
5. DetailMovie screen, yang muncul apabila card list film yang berada di movie screen ditekan. Detail screen berisi backdrop film, judul, tahun, rate, dan deskripsi singkat film.

Aplikasi ini menggunakan API dari https://www.themoviedb.org/settings/api


*Jika ingin mencoba aplikasi ini jangan lupa "npm install" agar library nya terdownload semua

*Noted: Gunakan Internet Saat Menggunakan Aplikasi Agar API bisa diakses

More info: email: amatsantoso27@gmail.com